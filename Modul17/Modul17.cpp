﻿
#include <iostream>
#include <math.h>
using namespace std;

class Example
{
public:
    Example() : a(10),b(20)
    {}
    Example(double _a,double _b) : a(_a),b(_b)
    {}
    void Show()
    {
        cout << a + b<<endl;
    }
private:
    double a, b;
};

class Vector
{
public:
    Vector() : x(0), y(0),z(60)
    {}
    Vector(double _x, double _y,double _z) : x(_x), y(_y),z(_z)
    {}
    void Module()
    {
        cout << sqrt(x * x + y * y + z * z)<<endl;
    }
private:
    double x,y,z;
};


int main()
{
    Example c;
    c.Show();
    Vector m;
    m.Module();
    system("pause");
}

